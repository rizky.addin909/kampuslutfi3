<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wisuda extends Model
{
    protected $fillable = [
        'id_siswa', 'email', 'nama_lengkap', 'nim', 'alamat_email', 'nohp', 'nama_ortu', 'prodi', 'tempat_lahir', 'tanggal_lahir', 'alamat_domisili', 'nik', 'file_kk', 'file_akte', 'tempat_pkl1', 'tanggal_pelaksanaan', 'judul_pkl1', 'tempat_pkl2', 'tanggal_pelaksanaan2', 'judul_pkl2', 'tempat_pkl3', 'tanggal_pelaksanaan3', 'judul_pkl3', 'judul_tugas_akhir', 'nama_pembimbing_tugas_akhir1', 'nama_pembimbing_tugas_akhir2', 'nama_pembimbing_tugas_akhir3', 'nama_penguji_tugas_akhir1', 'nama_penguji_tugas_akhir2', 'nama_penguji_tugas_akhir3', 'ipk_mahasiswa', 'file_bbp', 'file_bbm', 'file_ijazah', 'file_ktp', 'file_bpbw', 'file_bpktm', 'file_lpta', 'file_sppk', 'file_bplpkl', 'file_bbtp', 'file_bbta', 'file_beladiri', 'komentar'
    ];
}
