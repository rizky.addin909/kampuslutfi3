<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpOffice\PhpWord\TemplateProcessor;

use Response;
use PDF;
use Auth;
use App\User;
use App\Siswa;
use App\Wisuda;
use App\Berita;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $berita = Berita::get();

        return view('home/landing', compact('berita'));
    }
    public function index2()
    {
        $berita = Berita::get();

        return view('home/reset', compact('berita'));   
    }
    //controller admin start

    public function userview()
    {
        $users = User::get();
        $siswas = Siswa::get();
        return view('admin/UserRecord', compact('users', 'siswas'));
    }

    public function bukualumniview()
    {
        $users = User::get();
        $siswas = Siswa::get();
        $wisuda = Wisuda::orderBy('ipk_mahasiswa', 'DESC')->get();

        return view('admin/BukuAlumniRecord', compact('users', 'siswas', 'wisuda'));
    }
    public function cetakbukualumni()
    {   
        $wisuda = Wisuda::orderBy('ipk_mahasiswa', 'DESC')->get();
        $siswas = Siswa::get();
        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $phpWord->addParagraphStyle(
            'multipleTab',
            array(
                'tabs' => array(
                    new \PhpOffice\PhpWord\Style\Tab('left', 1550),
                    new \PhpOffice\PhpWord\Style\Tab('center', 3200),
                    new \PhpOffice\PhpWord\Style\Tab('right', 5300),
                )
            )
        );

        $phpWord->addParagraphStyle(
            'rightTab',
            array('tabs' => array(new \PhpOffice\PhpWord\Style\Tab('right', 9090)))
        );

        $phpWord->addParagraphStyle(
            'centerTab',
            array('tabs' => array(new \PhpOffice\PhpWord\Style\Tab('center', 4680)))
        );

        $section = $phpWord->addSection();
        
        $title1 = " KATA PENGANTAR ";
        $title2 = " DAFTAR ISI ";
        $title3 = " LULUSAN TERBAIK ";
        $title4 = " D3 TEKNIK KIMIA ";
        $title5 = " D3 TEKNIK MESIN ";
        $title6 = " D3 AKUNTANSI ";
        $title7 = " D3 BUDIDAYA TANAMAN PERKEBUNAN ";
        $title8 = " D4 BUDIDAYA TANAMAN PERKEBUNAN ";

        $desc1 = "
        \t Assalamualaikum wr.wb.
        \n
        \t Dengan mengucapkan puji dan syukur kehadirat Allah Yang Maha Pengasih dan Penyayang, Politeknik LPP diberi kesempatan dan kemudahan untuk melepaskan lulusan dalam acara wisuda ke-19 tahun 2019. Seluruh jajaran pengelola dan pelaksana mengucapkan selamat dan sukses bagi seluruh wisudawan yangtelah menyelesaikan studi di Politeknik LPP.
        \n
        \t Selanjutnya kami mengucapkan selamat berkarya dan men-gamalkan ilmu untuk kepentingan bangsa dan negara kita tercinta. Jadilah pribadi-pribadi yang jujur, ikhlas, mandiri, dan berkarakter ditengah masyarakat.
        \n
        \t Do’a kami menyertai seluruh wisudawan/wati.
        \n
        \t Wassalamu’alaikum wr.wb.
        ";

        $desc2="
        PENGANTAR.............….....…............................................................... i
        \n
        DAFTAR ISI........................................................................................ ii
        \n
        Lulusan Terbaik................................................................................ 1
        \n
        D3 Teknik Kimia................................................................................ 1
        \n
        D3 Teknik Mesin.............................................................................. 13
        \n
        D3 Akuntansi................................................................................... 18
        \n
        D3 Budidaya Tanaman Perkebunan.............................................. 21
        \n
        D4 Budidaya Tanaman Perkebunan.............................................. 35
        \n
        ";


        $section->addText($title1, array('name'=>'Arial','size' => 12,'bold' => true), array('align'=>'center'));
        $textlines = explode("\n", $desc1);
        $textrun = $section->addTextRun();
        $textrun->addText(array_shift($textlines));
        foreach($textlines as $line) {
            $textrun->addTextBreak();
            // maybe twice if you want to seperate the text
            // $textrun->addTextBreak(2);
            $textrun->addText($line, array('name'=>'Arial','size' => 9), array('align'=>'justify'));
        }
        $section->addPageBreak();

        $section->addText($title2, array('name'=>'Arial','size' => 12,'bold' => true), array('align'=>'center'));
        $textlines2 = explode("\n", $desc2);
        $textrun2 = $section->addTextRun();
        $textrun2->addText(array_shift($textlines2));
        foreach($textlines2 as $line2) {
            $textrun2->addTextBreak();
            // maybe twice if you want to seperate the text
            // $textrun->addTextBreak(2);
            $textrun2->addText($line2, array('name'=>'Arial','size' => 9), array('align'=>'center', 'bold' => true));
        }
        $section->addPageBreak();


        //start Lulusan terbaik
        $section->addText($title3, array('name'=>'Arial','size' => 12,'bold' => true), array('align'=>'center'));
        $table = $section->addTable();
        
        $textrun3 = $section->addTextRun();
        $cellRowSpan = array('vMerge' => 'restart');
        $cellRowContinue = array('vMerge' => 'continue');
        $cellColSpan = array('gridSpan' => 2);

        foreach ($wisuda as $wisu) {
            foreach ($siswas as $sis) {
                if ($sis->status == 'Lulus') {
                    if ($sis->id == $wisu->id_siswa) {
                        if ($wisu->ipk_mahasiswa > '3.50') {
                            $desc3="\n" . "Nama \t\t : ". $wisu->nama_lengkap. "\n" . "Nim \t\t\t : ". $wisu->nim. "\n". "Tempat Tanggal Lahir \t : ". $wisu->tempat_lahir . "," . $wisu->tanggal_lahir. "\n". "Alamat Asal \t\t : ". $wisu->alamat_domisili. "\n". "Nama OrangTua \t : ". $wisu->nama_ortu. "\n". "No. Telp \t\t : ". $wisu->nohp. "\n". "Email \t\t\t : ". $wisu->email. "\n". "Program Studi \t\t : ". $wisu->prodi. "\n". "IPK \t\t\t : ". $wisu->ipk_mahasiswa. "\n\n\n" ;

                            $textlines3 = explode("\n", $desc3);
                            $textrun3->addText(array_shift($textlines3));

                            if($wisu->file_bbm == null){
                                $path = public_path('uploads/avatars/user.png');
                                $table->addRow();
                                $table->addCell(2000, $cellRowSpan)->addImage($path, array('width'=>80, 'height'=>125, 'wrappingStyle' => 'square', 'positioning' => 'absolute','posHorizontalRel' => 'margin', 'posVerticalRel' => 'line',));
                                foreach($textlines3 as $line3) {
                                    
                                    $table->addCell(2000, $cellRowSpan)->addText($line3, array('name'=>'Arial','size' => 9), array('align'=>'justify', 'bold' => true));
                                    // maybe twice if you want to seperate the text
                                    // $textrun->addTextBreak(2);
                                    $table->addRow();
                                    $textrun3->addTextBreak();

                                    
                                }
                            }else{
                                $path = public_path('uploads/wisudas/'.$wisu->nim.'-'.$wisu->nama_lengkap.'/'.$wisu->file_bbm);
                                $table->addRow();
                                $table->addCell(2000, $cellRowSpan)->addImage($path, array('width'=>80, 'height'=>125, 'wrappingStyle' => 'square', 'positioning' => 'absolute','posHorizontalRel' => 'margin','posVerticalRel' => 'line',));
                                foreach($textlines3 as $line3) {
                                    $table->addCell(2000, $cellRowSpan)->addText($line3, array('name'=>'Arial','size' => 9), array('align'=>'justify', 'bold' => true));
                                    // maybe twice if you want to seperate the text
                                    // $textrun->addTextBreak(2);
                                    $table->addRow();
                                    $textrun3->addTextBreak();
                                }
                            }
                            
                        }   
                    }
                }
            }
            
        }
        //End Lulusan terbaik
        $section->addPageBreak();


        //start teknik kimia
        $section->addText($title4, array('name'=>'Arial','size' => 12,'bold' => true), array('align'=>'center'));
        $table = $section->addTable();
        
        $textrun3 = $section->addTextRun();
        $cellRowSpan = array('vMerge' => 'restart');
        $cellRowContinue = array('vMerge' => 'continue');
        $cellColSpan = array('gridSpan' => 2);

        foreach ($wisuda as $wisu) {
            foreach ($siswas as $sis) {
                if ($sis->status == 'Lulus') {
                    if ($sis->id == $wisu->id_siswa) {
                        if ($wisu->prodi == 'Teknik Kimia') {
                            if ($wisu->ipk_mahasiswa <='3.50') {
                                $desc3="\n" . "Nama \t\t : ". $wisu->nama_lengkap. "\n" . "Nim \t\t\t : ". $wisu->nim. "\n". "Tempat Tanggal Lahir \t : ". $wisu->tempat_lahir . "," . $wisu->tanggal_lahir. "\n". "Alamat Asal \t\t : ". $wisu->alamat_domisili. "\n". "Nama OrangTua \t : ". $wisu->nama_ortu. "\n". "No. Telp \t\t : ". $wisu->nohp. "\n". "Email \t\t\t : ". $wisu->email. "\n". "Program Studi \t\t : ". $wisu->prodi. "\n". "IPK \t\t\t : ". $wisu->ipk_mahasiswa. "\n\n\n" ;

                                $textlines3 = explode("\n", $desc3);
                                $textrun3->addText(array_shift($textlines3));

                                if($wisu->file_bbm == null){
                                    $path = public_path('uploads/avatars/user.png');
                                    $table->addRow();
                                    $table->addCell(2000, $cellRowSpan)->addImage($path, array('width'=>80, 'height'=>125, 'wrappingStyle' => 'square', 'positioning' => 'absolute','posHorizontalRel' => 'margin', 'posVerticalRel' => 'line',));
                                    foreach($textlines3 as $line3) {
                                        
                                        $table->addCell(2000, $cellRowSpan)->addText($line3, array('name'=>'Arial','size' => 9), array('align'=>'justify', 'bold' => true));
                                        // maybe twice if you want to seperate the text
                                        // $textrun->addTextBreak(2);
                                        $table->addRow();
                                        $textrun3->addTextBreak();

                                        
                                    }
                                }else{
                                    $path = public_path('uploads/wisudas/'.$wisu->nim.'-'.$wisu->nama_lengkap.'/'.$wisu->file_bbm);
                                    $table->addRow();
                                    $table->addCell(2000, $cellRowSpan)->addImage($path, array('width'=>80, 'height'=>125, 'wrappingStyle' => 'square', 'positioning' => 'absolute','posHorizontalRel' => 'margin','posVerticalRel' => 'line',));
                                    foreach($textlines3 as $line3) {
                                        $table->addCell(2000, $cellRowSpan)->addText($line3, array('name'=>'Arial','size' => 9), array('align'=>'justify', 'bold' => true));
                                        // maybe twice if you want to seperate the text
                                        // $textrun->addTextBreak(2);
                                        $table->addRow();
                                        $textrun3->addTextBreak();
                                    }
                                }
                            }
                            
                        }   
                    }
                }
            }
            
        }
        //End teknik kimia
        $section->addPageBreak();


        //start teknik mesin
        $section->addText($title5, array('name'=>'Arial','size' => 12,'bold' => true), array('align'=>'center'));
        $table = $section->addTable();
        
        $textrun3 = $section->addTextRun();
        $cellRowSpan = array('vMerge' => 'restart');
        $cellRowContinue = array('vMerge' => 'continue');
        $cellColSpan = array('gridSpan' => 2);

        foreach ($wisuda as $wisu) {
            foreach ($siswas as $sis) {
                if ($sis->status == 'Lulus') {
                    if ($sis->id == $wisu->id_siswa) {
                        if ($wisu->prodi == 'Teknik Mesin') {
                            if ($wisu->ipk_mahasiswa <='3.50') {
                                $desc3="\n" . "Nama \t\t : ". $wisu->nama_lengkap. "\n" . "Nim \t\t\t : ". $wisu->nim. "\n". "Tempat Tanggal Lahir \t : ". $wisu->tempat_lahir . "," . $wisu->tanggal_lahir. "\n". "Alamat Asal \t\t : ". $wisu->alamat_domisili. "\n". "Nama OrangTua \t : ". $wisu->nama_ortu. "\n". "No. Telp \t\t : ". $wisu->nohp. "\n". "Email \t\t\t : ". $wisu->email. "\n". "Program Studi \t\t : ". $wisu->prodi. "\n". "IPK \t\t\t : ". $wisu->ipk_mahasiswa. "\n\n\n" ;

                                $textlines3 = explode("\n", $desc3);
                                $textrun3->addText(array_shift($textlines3));

                                if($wisu->file_bbm == null){
                                    $path = public_path('uploads/avatars/user.png');
                                    $table->addRow();
                                    $table->addCell(2000, $cellRowSpan)->addImage($path, array('width'=>80, 'height'=>125, 'wrappingStyle' => 'square', 'positioning' => 'absolute','posHorizontalRel' => 'margin', 'posVerticalRel' => 'line',));
                                    foreach($textlines3 as $line3) {
                                        
                                        $table->addCell(2000, $cellRowSpan)->addText($line3, array('name'=>'Arial','size' => 9), array('align'=>'justify', 'bold' => true));
                                        // maybe twice if you want to seperate the text
                                        // $textrun->addTextBreak(2);
                                        $table->addRow();
                                        $textrun3->addTextBreak();

                                        
                                    }
                                }else{
                                    $path = public_path('uploads/wisudas/'.$wisu->nim.'-'.$wisu->nama_lengkap.'/'.$wisu->file_bbm);
                                    $table->addRow();
                                    $table->addCell(2000, $cellRowSpan)->addImage($path, array('width'=>80, 'height'=>125, 'wrappingStyle' => 'square', 'positioning' => 'absolute','posHorizontalRel' => 'margin','posVerticalRel' => 'line',));
                                    foreach($textlines3 as $line3) {
                                        $table->addCell(2000, $cellRowSpan)->addText($line3, array('name'=>'Arial','size' => 9), array('align'=>'justify', 'bold' => true));
                                        // maybe twice if you want to seperate the text
                                        // $textrun->addTextBreak(2);
                                        $table->addRow();
                                        $textrun3->addTextBreak();
                                    }
                                }
                            }
                            
                        }   
                    }
                }
            }
            
        }
        //End teknik mesin
        $section->addPageBreak();


        //start akuntansi
        $section->addText($title6, array('name'=>'Arial','size' => 12,'bold' => true), array('align'=>'center'));
        $table = $section->addTable();
        
        $textrun3 = $section->addTextRun();
        $cellRowSpan = array('vMerge' => 'restart');
        $cellRowContinue = array('vMerge' => 'continue');
        $cellColSpan = array('gridSpan' => 2);

        foreach ($wisuda as $wisu) {
            foreach ($siswas as $sis) {
                if ($sis->status == 'Lulus') {
                    if ($sis->id == $wisu->id_siswa) {
                        if ($wisu->prodi == 'Akuntansi') {
                            if ($wisu->ipk_mahasiswa <='3.50') {
                                $desc3="\n" . "Nama \t\t : ". $wisu->nama_lengkap. "\n" . "Nim \t\t\t : ". $wisu->nim. "\n". "Tempat Tanggal Lahir \t : ". $wisu->tempat_lahir . "," . $wisu->tanggal_lahir. "\n". "Alamat Asal \t\t : ". $wisu->alamat_domisili. "\n". "Nama OrangTua \t : ". $wisu->nama_ortu. "\n". "No. Telp \t\t : ". $wisu->nohp. "\n". "Email \t\t\t : ". $wisu->email. "\n". "Program Studi \t\t : ". $wisu->prodi. "\n". "IPK \t\t\t : ". $wisu->ipk_mahasiswa. "\n\n\n" ;

                                $textlines3 = explode("\n", $desc3);
                                $textrun3->addText(array_shift($textlines3));

                                if($wisu->file_bbm == null){
                                    $path = public_path('uploads/avatars/user.png');
                                    $table->addRow();
                                    $table->addCell(2000, $cellRowSpan)->addImage($path, array('width'=>80, 'height'=>125, 'wrappingStyle' => 'square', 'positioning' => 'absolute','posHorizontalRel' => 'margin', 'posVerticalRel' => 'line',));
                                    foreach($textlines3 as $line3) {
                                        
                                        $table->addCell(2000, $cellRowSpan)->addText($line3, array('name'=>'Arial','size' => 9), array('align'=>'justify', 'bold' => true));
                                        // maybe twice if you want to seperate the text
                                        // $textrun->addTextBreak(2);
                                        $table->addRow();
                                        $textrun3->addTextBreak();

                                        
                                    }
                                }else{
                                    $path = public_path('uploads/wisudas/'.$wisu->nim.'-'.$wisu->nama_lengkap.'/'.$wisu->file_bbm);
                                    $table->addRow();
                                    $table->addCell(2000, $cellRowSpan)->addImage($path, array('width'=>80, 'height'=>125, 'wrappingStyle' => 'square', 'positioning' => 'absolute','posHorizontalRel' => 'margin','posVerticalRel' => 'line',));
                                    foreach($textlines3 as $line3) {
                                        $table->addCell(2000, $cellRowSpan)->addText($line3, array('name'=>'Arial','size' => 9), array('align'=>'justify', 'bold' => true));
                                        // maybe twice if you want to seperate the text
                                        // $textrun->addTextBreak(2);
                                        $table->addRow();
                                        $textrun3->addTextBreak();
                                    }
                                }
                            }
                            
                        }   
                    }
                }
            }
            
        }
        //End akuntansi
        $section->addPageBreak();


        //start d3 budidaya tanaman perkebunan
        $section->addText($title7, array('name'=>'Arial','size' => 12,'bold' => true), array('align'=>'center'));
        $table = $section->addTable();
        
        $textrun3 = $section->addTextRun();
        $cellRowSpan = array('vMerge' => 'restart');
        $cellRowContinue = array('vMerge' => 'continue');
        $cellColSpan = array('gridSpan' => 2);

        foreach ($wisuda as $wisu) {
            foreach ($siswas as $sis) {
                if ($sis->status == 'Lulus') {
                    if ($sis->id == $wisu->id_siswa) {
                        if ($wisu->prodi == 'Budidaya Tanaman Perkebunan D3') {
                            if ($wisu->ipk_mahasiswa <='3.50') {
                                $desc3="\n" . "Nama \t\t : ". $wisu->nama_lengkap. "\n" . "Nim \t\t\t : ". $wisu->nim. "\n". "Tempat Tanggal Lahir \t : ". $wisu->tempat_lahir . "," . $wisu->tanggal_lahir. "\n". "Alamat Asal \t\t : ". $wisu->alamat_domisili. "\n". "Nama OrangTua \t : ". $wisu->nama_ortu. "\n". "No. Telp \t\t : ". $wisu->nohp. "\n". "Email \t\t\t : ". $wisu->email. "\n". "Program Studi \t\t : ". $wisu->prodi. "\n". "IPK \t\t\t : ". $wisu->ipk_mahasiswa. "\n\n\n" ;

                                $textlines3 = explode("\n", $desc3);
                                $textrun3->addText(array_shift($textlines3));

                                if($wisu->file_bbm == null){
                                    $path = public_path('uploads/avatars/user.png');
                                    $table->addRow();
                                    $table->addCell(2000, $cellRowSpan)->addImage($path, array('width'=>80, 'height'=>125, 'wrappingStyle' => 'square', 'positioning' => 'absolute','posHorizontalRel' => 'margin', 'posVerticalRel' => 'line',));
                                    foreach($textlines3 as $line3) {
                                        
                                        $table->addCell(2000, $cellRowSpan)->addText($line3, array('name'=>'Arial','size' => 9), array('align'=>'justify', 'bold' => true));
                                        // maybe twice if you want to seperate the text
                                        // $textrun->addTextBreak(2);
                                        $table->addRow();
                                        $textrun3->addTextBreak();

                                        
                                    }
                                }else{
                                    $path = public_path('uploads/wisudas/'.$wisu->nim.'-'.$wisu->nama_lengkap.'/'.$wisu->file_bbm);
                                    $table->addRow();
                                    $table->addCell(2000, $cellRowSpan)->addImage($path, array('width'=>80, 'height'=>125, 'wrappingStyle' => 'square', 'positioning' => 'absolute','posHorizontalRel' => 'margin','posVerticalRel' => 'line',));
                                    foreach($textlines3 as $line3) {
                                        $table->addCell(2000, $cellRowSpan)->addText($line3, array('name'=>'Arial','size' => 9), array('align'=>'justify', 'bold' => true));
                                        // maybe twice if you want to seperate the text
                                        // $textrun->addTextBreak(2);
                                        $table->addRow();
                                        $textrun3->addTextBreak();
                                    }
                                }
                            }
                            
                        }   
                    }
                }
            }
            
        }
        //End d3 budidaya tanaman perkebunan
        $section->addPageBreak();


        //start d4 budidaya tanaman perkebunan
        $section->addText($title8, array('name'=>'Arial','size' => 12,'bold' => true), array('align'=>'center'));
        $table = $section->addTable();
        
        $textrun3 = $section->addTextRun();
        $cellRowSpan = array('vMerge' => 'restart');
        $cellRowContinue = array('vMerge' => 'continue');
        $cellColSpan = array('gridSpan' => 2);

        foreach ($wisuda as $wisu) {
            foreach ($siswas as $sis) {
                if ($sis->status == 'Lulus') {
                    if ($sis->id == $wisu->id_siswa) {
                        if ($wisu->prodi == 'Budidaya Tanaman Perkebunan D4') {
                            if ($wisu->ipk_mahasiswa <='3.50') {
                                $desc3="\n" . "Nama \t\t : ". $wisu->nama_lengkap. "\n" . "Nim \t\t\t : ". $wisu->nim. "\n". "Tempat Tanggal Lahir \t : ". $wisu->tempat_lahir . "," . $wisu->tanggal_lahir. "\n". "Alamat Asal \t\t : ". $wisu->alamat_domisili. "\n". "Nama OrangTua \t : ". $wisu->nama_ortu. "\n". "No. Telp \t\t : ". $wisu->nohp. "\n". "Email \t\t\t : ". $wisu->email. "\n". "Program Studi \t\t : ". $wisu->prodi. "\n". "IPK \t\t\t : ". $wisu->ipk_mahasiswa. "\n\n\n" ;

                                $textlines3 = explode("\n", $desc3);
                                $textrun3->addText(array_shift($textlines3));

                                if($wisu->file_bbm == null){
                                    $path = public_path('uploads/avatars/user.png');
                                    $table->addRow();
                                    $table->addCell(2000, $cellRowSpan)->addImage($path, array('width'=>80, 'height'=>125, 'wrappingStyle' => 'square', 'positioning' => 'absolute','posHorizontalRel' => 'margin', 'posVerticalRel' => 'line',));
                                    foreach($textlines3 as $line3) {
                                        
                                        $table->addCell(2000, $cellRowSpan)->addText($line3, array('name'=>'Arial','size' => 9), array('align'=>'justify', 'bold' => true));
                                        // maybe twice if you want to seperate the text
                                        // $textrun->addTextBreak(2);
                                        $table->addRow();
                                        $textrun3->addTextBreak();

                                        
                                    }
                                }else{
                                    $path = public_path('uploads/wisudas/'.$wisu->nim.'-'.$wisu->nama_lengkap.'/'.$wisu->file_bbm);
                                    $table->addRow();
                                    $table->addCell(2000, $cellRowSpan)->addImage($path, array('width'=>80, 'height'=>125, 'wrappingStyle' => 'square', 'positioning' => 'absolute','posHorizontalRel' => 'margin','posVerticalRel' => 'line',));
                                    foreach($textlines3 as $line3) {
                                        $table->addCell(2000, $cellRowSpan)->addText($line3, array('name'=>'Arial','size' => 9), array('align'=>'justify', 'bold' => true));
                                        // maybe twice if you want to seperate the text
                                        // $textrun->addTextBreak(2);
                                        $table->addRow();
                                        $textrun3->addTextBreak();
                                    }
                                }
                            }
                            
                        }   
                    }
                }
            }
            
        }
        //End d4 budidaya tanaman perkebunan
        

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        try {
            $objWriter->save(storage_path('BukuAlumni.docx'));
        } catch (Exception $e) {
        }
        return response()->download(storage_path('BukuAlumni.docx'))->deleteFileAfterSend(true);
    }

    public function wisudaview()
    {
        $users = User::get();

        $siswas = Siswa::get();
        $wisuda = Wisuda::get();
        return view('admin/WisudaRecord', compact('users', 'siswas', 'wisuda'));
    }

    public function beritaview()
    {
        $users = User::get();

        $siswas = Siswa::get();
        $berita = Berita::get();
        $nil = 1;
        return view('admin/BeritaRecord', compact('users', 'siswas', 'berita', 'nil'));
    }

    public function tambahberitaview()
    {
        $users = User::get();

        $siswas = Siswa::get();
        return view('admin/TambahBerita', compact('users', 'siswas'));
    }
    //controller admin end


    //controller admin super start
    public function userviewsuper()
    {
        $users = User::get();

        $siswas = Siswa::get();
        return view('adminsuper/UserRecord', compact('users', 'siswas'));
    }

    public function wisudaviewsuper()
    {
        $users = User::get();

        $siswas = Siswa::get();
        $wisuda = Wisuda::get();
        return view('adminsuper/WisudaRecord', compact('users', 'siswas', 'wisuda'));
    }

    public function beritaviewsuper()
    {
        $users = User::get();

        $siswas = Siswa::get();
        $berita = Berita::get();
        $nil = 1;
        return view('adminsuper/BeritaRecord', compact('users', 'siswas', 'berita', 'nil'));
    }

    public function tambahberitaviewsuper()
    {
        $users = User::get();

        $siswas = Siswa::get();
        return view('adminsuper/TambahBerita', compact('users', 'siswas'));
    }

    //controller admin super end

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    //controller admin start
    public function edituser(Request $request, $id)
    {
        $userid = $request->iduser;
        $users = User::get();
        $siswas = Siswa::where('user_id', $userid)->get();
        return view('admin/EditUser', compact('users', 'id', 'siswas'));
    }

    public function editwisuda(Request $request, $id)
    {
        $wisudaid = $request->idsiswa;
        $wisuda = Wisuda::where('id_siswa', $wisudaid)->get();
        $siswas = Siswa::where('id', $wisudaid)->get();
        $users = User::get();
        return view('admin/EditWisuda', compact('wisuda', 'id', 'siswas', 'users'));
    }
    public function editwisudacetak(Request $request, $id)
    {
        $wisudaid = $request->idsiswa;
        $wisuda = Wisuda::where('id_siswa', $wisudaid)->get();
        $wisudasa = Wisuda::where('id_siswa', $wisudaid)->get('nim')->first();
        $siswas = Siswa::where('id', $wisudaid)->get();
        $users = User::get();
        $pdf = PDF::loadview('admin/WisudaCetak_pdf',['wisuda'=>$wisuda, 'siswas'=>$siswas, 'users'=>$users, 'id'=>$id]);
        $pdf->setPaper('A4');
        return $pdf->download('DataMahasiswa-'.$wisudasa['nim'].'.pdf');
    }

    public function editfotocetak(Request $request, $id)
    {
        $wisudaid = $request->idsiswa;
        $wisuda = Wisuda::where('id_siswa', $wisudaid)->get();
        $wisudasa = Wisuda::where('id_siswa', $wisudaid)->get('file_bbp')->first();
        $foldernamenim1 = Wisuda::where('id_siswa', $wisudaid)->get('nim')->first();
        $foldername1 = Wisuda::where('id_siswa', $wisudaid)->get('nama_lengkap')->first();
        $filepath = public_path('uploads/wisudas/' . $foldernamenim1['nim'] . '-' . $foldername1['nama_lengkap']).'/'.$wisudasa['file_bbp'];
        return Response::download($filepath);
    }
    public function editfoto2cetak(Request $request, $id)
    {
        $wisudaid = $request->idsiswa;
        $wisuda = Wisuda::where('id_siswa', $wisudaid)->get();
        $wisudasa = Wisuda::where('id_siswa', $wisudaid)->get('file_bbm')->first();
        $foldernamenim1 = Wisuda::where('id_siswa', $wisudaid)->get('nim')->first();
        $foldername1 = Wisuda::where('id_siswa', $wisudaid)->get('nama_lengkap')->first();
        $filepath = public_path('uploads/wisudas/' . $foldernamenim1['nim'] . '-' . $foldername1['nama_lengkap']).'/'.$wisudasa['file_bbm'];
        return Response::download($filepath);
    }


    public function editberita(Request $request, $id)
    {
        $userid = $request->idberita;
        $users = User::get();
        $siswas = Siswa::where('user_id', $userid)->get();
        $berita = berita::where('id', $userid)->get();
        return view('admin/EditBerita', compact('users', 'id', 'berita', 'siswas'));
    }

    public function tambahberita(Request $request)
    {
        $berita = new Berita;
        $berita->user_id = auth()->user()->id;
        $berita->berita = $request->berita;
        $berita->save();
        return redirect()->route('viewberita')->with('success_message', 'Deskripsi berhasil diperbarui');
    }

    //controller admin end

    //controller admin super start
    public function editusersuper(Request $request, $id)
    {
        $userid = $request->iduser;
        $users = User::get();
        $siswas = Siswa::where('user_id', $userid)->get();
        return view('adminsuper/EditUser', compact('users', 'id', 'siswas'));
    }

    public function editwisudasuper(Request $request, $id)
    {
        $wisudaid = $request->idsiswa;
        $wisuda = Wisuda::where('id_siswa', $wisudaid)->get();
        $siswas = Siswa::where('id', $wisudaid)->get();
        $users = User::get();
        return view('adminsuper/EditWisuda', compact('wisuda', 'id', 'siswas', 'users'));
    }

    public function editberitasuper(Request $request, $id)
    {
        $userid = $request->idberita;
        $users = User::get();
        $siswas = Siswa::where('user_id', $userid)->get();
        $berita = berita::where('id', $userid)->get();
        return view('adminsuper/EditBerita', compact('users', 'id', 'berita', 'siswas'));
    }

    public function tambahberitasuper(Request $request)
    {
        $berita = new Berita;
        $berita->user_id = auth()->user()->id;
        $berita->berita = $request->berita;
        $berita->save();
        return redirect()->route('viewberitasuper')->with('success_message', 'Deskripsi berhasil diperbarui');
    }

    //controller admin super end

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //controller admin start

    public function update(Request $request, User $User)
    {
        $userid = $request->iduser;
        User::where('id', $userid)->update([
            'name' => $request->username,
            'email' => $request->email,
        ]);

        Siswa::where('user_id', $userid)->update([
            'status' => $request->stat,
        ]);
        return redirect()->route('viewuser')->with('success_message', 'Deskripsi berhasil diperbarui');
    }

    public function updatewisuda(Request $request, User $User)
    {
        $userid = $request->sisid;
        Siswa::where('id', $userid)->update([
            'status' => $request->stat,
        ]);
        Wisuda::where('id_siswa', $userid)->update([
            'komentar' => $request->komentar,
        ]);
        return redirect()->route('viewwisuda')->with('success_message', 'Deskripsi berhasil diperbarui');
    }

    public function updateberita(Request $request, User $User)
    {
        $userid = $request->berid;
        Berita::where('id', $userid)->update([
            'berita' => $request->berita,
        ]);
        return redirect()->route('viewberita')->with('success_message', 'Deskripsi berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function deleteberita($id)
    {
        Berita::where('id',$id)->delete();
        return redirect()->route('viewberita')->with('success_message', 'Deskripsi berhasil dihapus');
    }

    //controller admin end

    //controller admin super start

    public function updatesuper(Request $request, User $User)
    {
        $userid = $request->iduser;
        User::where('id', $userid)->update([
            'name' => $request->username,
            'email' => $request->email,
            'status' => $request->status,
        ]);

        Siswa::where('user_id', $userid)->update([
            'status' => $request->stat,
        ]);
        return redirect()->route('viewusersuper')->with('success_message', 'Deskripsi berhasil diperbarui');
    }

    public function updatewisudasuper(Request $request, User $User)
    {
        $userid = $request->sisid;
        Siswa::where('id', $userid)->update([
            'status' => $request->stat,
        ]);
        Wisuda::where('id_siswa', $userid)->update([
            'komentar' => $request->komentar,
        ]);
        return redirect()->route('viewwisudasuper')->with('success_message', 'Deskripsi berhasil diperbarui');
    }

    public function updateberitasuper(Request $request, User $User)
    {
        $userid = $request->berid;
        Berita::where('id', $userid)->update([
            'berita' => $request->berita,
        ]);
        return redirect()->route('viewberitasuper')->with('success_message', 'Deskripsi berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function deleteberitasuper($id)
    {
        Berita::where('id',$id)->delete();
        return redirect()->route('viewberitasuper')->with('success_message', 'Deskripsi berhasil dihapus');
    }

    //controller admin super end

    public function destroy($id)
    {
        //
    }
}
