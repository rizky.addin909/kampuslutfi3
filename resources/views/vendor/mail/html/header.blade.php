<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'Politeknik_Perkebunan_Yogyakarta')
<h2>Politeknik Perkebunan Yogyakarta</h2>
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
