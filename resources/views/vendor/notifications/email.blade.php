@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level === 'error')
# @lang('Whoops!')
@else
# @lang('Haii!')
@endif
@endif

{{-- Intro Lines --}}
@lang('Anda menerima email ini karena kami menerima permintaan pengaturan ulang kata sandi untuk akun Anda.')

{{-- Action Button --}}
@isset($actionText)
<?php
    switch ($level) {
        case 'success':
        case 'error':
            $color = $level;
            break;
        default:
            $color = 'primary';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@endisset

{{-- Outro Lines --}}
@lang('Link pengaturan ulang kata sandi ini akan kedaluwarsa dalam 60 menit.')
@lang('Jika Anda tidak meminta pengaturan ulang kata sandi, tidak ada tindakan lebih lanjut yang diperlukan.    ')

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
@lang('AdminLPP'),<br>
{{ config('app.name') }}
@endif

{{-- Subcopy --}}
@isset($actionText)
@slot('subcopy')
@lang(
    "Jika Anda mengalami masalah saat mengeklik \":actionText\" tombol, salin dan tempel URL di bawah\n".
    'ke browser Anda:',
    [
        'actionText' => $actionText,
    ]
) <span class="break-all">[{{ $displayableActionUrl }}]({{ $actionUrl }})</span>
@endslot
@endisset
@endcomponent
