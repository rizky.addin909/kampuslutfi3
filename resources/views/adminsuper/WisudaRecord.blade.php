@extends('../layout/applog')

@section('title','Nama Website')


<!-- Page content -->
@section('content')
  <!-- Header -->
  <header class="w3-container" style="padding-top:22px">
    <h4><span>Data <strong>Yudisium</strong></span><br><h4>
  </header>

  <div class="w3-panel">
    <div class="w3-row-padding" style="margin:0 -16px">
      <div class="w3-twothird">
        <h5>Feeds</h5>
        <table class="w3-table w3-striped w3-white">
          @foreach( $wisuda as $wisud)
          <tr>
            <td>{{$wisud->nama_lengkap}}</i></td>
            <td>{{$wisud->email}}</td>
            @foreach( $siswas as $siswa)
            @if($wisud->id_siswa == $siswa->id)
            <td><i>{{$siswa->status}}</i></td>            
            @endif
            @endforeach
            <td>{{$wisud->komentar}}</td>
            <td>
              <form action="{{ route('editwisudasuper', $wisud->id_siswa) }}" method="get">
                @csrf
                <input id="iduser" type="hidden"  name="idsiswa" value="{{$wisud->id_siswa}}">
                <input type="submit" value="edit">
              </form>
            </td>
          </tr>

          @endforeach
          
        </table>
      </div>
    </div>
  </div>
  @endsection

  @section('footer')
  <!-- Footer -->
  <footer class="w3-container w3-padding-16 w3-light-grey">
    <center><p>Copyright © 1994 - 2020 LPP YOGYAKARTA. </p></center>
  </footer>
@endsection
