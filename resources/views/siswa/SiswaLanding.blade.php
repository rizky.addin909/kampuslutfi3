@extends('../layout/applog')

@section('title','Nama Website')


<!-- Page content -->
@section('content')
@foreach( $users as $user)
@if($user->id == auth()->user()->id)
  <!-- Header -->
  <header class="w3-container" style="padding-top:22px">
    <h4><span>Profile <strong>Mahasiswa</strong></span><br><h4>
  </header>

  <div class="w3-panel">
    <div class="w3-row-padding" style="margin:0 -16px">
      <div class="w3-twothird">
        <h5>Ubah Data</h5>
        <table class="w3-table w3-striped w3-white">
          @foreach( $siswa as $sis)
          @if($sis->user_id == auth()->user()->id)
          <form action="{{ route('avatarsa') }}" method="post" enctype="multipart/form-data">
                <tr>
                  <td>    
                      @csrf
                      <p>Foto Anda : </p>
                      <input type="file" name="avatar">
                      <input type="submit" class="pull-right btn btn-sm btn-primary" onClick="window.location.reload()" value="submit">
                  </td>
                </tr>
              </form>
              <form action="{{ route('siswa.update') }}" method="post">
                @csrf
                <tr>
                  <td>
                    <p>Nama Depan : </p>
                    <input class="uk-input uk-width-1-3 uk-margin-large-left" type="text" placeholder="Nama Depan" name="namadep" value="{{ $sis->nama_depan }}">
                  </td>
                </tr>

                <tr>
                  <td>
                    <p>Nama belakang : </p>
                    <input class="uk-input uk-width-1-2 uk-margin-large-left" type="text" placeholder="Nama Belakang" name="namabel" value="{{ $sis->nama_belakang }}">
                  </td>
                </tr>

                <tr>
                  <td>
                    <p>Email : </p>
                    <input class="uk-input uk-width-1-1 uk-margin-large-left" type="text" placeholder="Alamat Email" name="email" value="{{ $sis->email }}">
                  </td>
                </tr>

                <tr>
                  <td>
                    <p>No Telpon : </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="No Telepon" name="notelp" value="{{ $sis->telepon }}">
                  </td>
                </tr>

                <tr>
                  <td>
                    <p>Alamat : </p>
                    <textarea class="uk-textarea uk-width-1-1 uk-margin-large-left" rows="5" name="alamat" placeholder="Alamat Rumah"> {{ $sis->alamat }} </textarea>

                  </td>
                </tr>

                <tr>
                  <td>
                    <p>Status :  </p>
                    <p>{{ $sis->status }} </p>
                  </td>
                </tr>

                <tr>
                  <td>
                    <input type="submit" value="ubah">
                  </td>
                </tr>
              </form>
          @endif
          @endforeach
          
        </table>
      </div>
    </div>
  </div>

  @endif
  @endforeach
  @endsection

  @section('footer')
  <!-- Footer -->
  <footer class="w3-container w3-padding-16 w3-light-grey">
    <center><p>Copyright © 1994 - 2020 LPP YOGYAKARTA. </p></center>
  </footer>
@endsection
