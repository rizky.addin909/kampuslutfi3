<!DOCTYPE html>
<html>
<head>
  <title>Data Mahasiswa</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <style>
    @page {
                margin: 0cm 0cm;
            }

            /**
            * Define the real margins of the content of your PDF
            * Here you will fix the margins of the header and footer
            * Of your background image.
            **/
            body {
                margin-top:    2cm;
                margin-bottom: 1cm;
                margin-left:   2cm;
                margin-right:  2cm;
            }

    #watermark {
                position: fixed;
                bottom:   0px;
                left:     0px;

                /** Change image dimensions**/
                width:    210mm;
                height:   297mm;

                /** Your watermark should be behind every content**/
                z-index:  -1000;
            }
            #watermarks {
                position: fixed;

                /** 
                    Set a position in the page for your image
                    This should center it vertically
                **/
                bottom:   0px;
                left:     6.5cm;

                /** Change image dimensions**/
                width:    8cm;
                height:   2cm;

                /** Your watermark should be behind every content**/
                z-index:  -1000;
            }
    </style>

</head>
<body>
  <div id="watermark">
    <img src="image/background/layout.jpg" height="100%" width="100%" />
  </div>

  <div id="watermarks">
    <center><p>Data Mahasiswa Politeknik LPP Tahun 2019</p></center>
  </div>

  <main> 
  <style type="text/css">
    table tr td,
    table tr th{
      font-size: 9pt;
    }
  </style>
  <style>
        table {
          border-collapse: collapse;
          border: 0px solid black;
        }
        th,td {
            font-size: 15px;
        }
          .garis_atas{
            border-bottom: 3px black solid;
            height: 5px;
            width: 0px;
          }
          .garis_bawah{
            border-bottom: 3px black solid;
            height: 5px;
            width: 0px;
          }
        tr:hover {background-color: lightblue;}
</style>

  <table align="left">
      @foreach( $wisuda as $wisud)
      @if($wisud->id_siswa == $id)
        <tr>
          <td colspan="8" style="font-size: 30px; text-align: center;"><p><b>Data Mahasiswa</b></p></td>
        </tr>

        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Email :  {{ $wisud->email }}</p>
          </td>
        </tr>
        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Nama Lengkap : {{ $wisud->nama_lengkap }} </p>
            
          </td>
        </tr>

        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Nim : {{ $wisud->nim }} </p>
             
          </td>
        </tr>

        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Alamat Email : {{ $wisud->alamat_email }} </p>
            
          </td>
        </tr>
        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>No Handphone (Masih Aktif) : {{ $wisud->nohp }} </p>
            
          </td>
        </tr>
        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Nama Orang Tua :  {{ $wisud->nama_ortu }} </p>
            
          </td>
        </tr>
        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Program Studi : {{ $wisud->prodi }} </p>
           
          </td>
        </tr>
        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Tempat Lahir (Sesuai Kartu Keluarga) : {{ $wisud->tempat_lahir }} </p>
           
          </td>
        </tr>
        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Tanggal Lahir : {{ $wisud->tanggal_lahir }} </p>
            
          </td>
        </tr>

        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Alamat Domisili (Asal) : {{ $wisud->alamat_domisili }} </p>
           
          </td>
        </tr>

        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>NIK (Nomor Induk Kependudukan) : {{ $wisud->nik }} </p>
           
          </td>
        </tr>
        
        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Tempat PKL I : {{ $wisud->tempat_pkl1 }} </p>
           
          </td>
        </tr>

        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Tanggal Pelaksanaan I : {{ $wisud->tanggal_pelaksanaan }} </p>
            
          </td>
        </tr>

        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Judul PKL I : {{ $wisud->judul_pkl1 }}</p>
            
          </td>
        </tr>

        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Tempat PKL II : {{ $wisud->tempat_pkl2 }}</p>
            
          </td>
        </tr>

        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Tanggal Pelaksanaan II : {{ $wisud->tanggal_pelaksanaan2 }} </p>
           
          </td>
        </tr>

        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Judul PKL II : {{ $wisud->judul_pkl2 }}</p>
           
          </td>
        </tr>

        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Tempat PKL III : {{ $wisud->tempat_pkl3 }} </p>
            
          </td>
        </tr>

        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Tanggal Pelaksanaan III : {{ $wisud->tanggal_pelaksanaan3 }}</p>
            
          </td>
        </tr>

        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Judul PKL III : {{ $wisud->judul_pkl3 }} </p>
            
          </td>
        </tr>

        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Judul Tugas Akhir : {{ $wisud->judul_tugas_akhir }}</p>
            
          </td>
        </tr>

        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Nama Pembimbing Tugas Akhir (1) : {{ $wisud->nama_pembimbing_tugas_akhir1 }} </p>
            
          </td>
        </tr>
        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Nama Pembimbing Tugas Akhir (2) : {{ $wisud->nama_pembimbing_tugas_akhir2 }}</p>
            
          </td>
        </tr>
        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Nama Pembimbing Tugas Akhir (3) : {{ $wisud->nama_pembimbing_tugas_akhir3 }} </p>
            
          </td>
        </tr>

        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Nama Penguji Tugas Akhir (1) : {{ $wisud->nama_penguji_tugas_akhir1 }}</p>
            
          </td>
        </tr>
        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Nama Penguji Tugas Akhir (2) : {{ $wisud->nama_penguji_tugas_akhir2 }}</p>
            
          </td>
        </tr>
        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Nama Penguji Tugas Akhir (3) : {{ $wisud->nama_penguji_tugas_akhir3 }}</p>
            
          </td>
        </tr>

        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Ipk Mahasiswa : {{ $wisud->ipk_mahasiswa }} </p>
           
          </td>
        </tr>

        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Upload Foto Berwarna Background Putih</p>
           <p> <img src="uploads/wisudas/{{$wisud->nama_lengkap}}/{{$wisud->file_bbp}}" height="250px" width="250px"> </p> 
          </td>
        </tr>

        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Upload Foto Berwarna Background Merah </p>
            <p> <img src="uploads/wisudas/{{$wisud->nama_lengkap}}/{{$wisud->file_bbm}}" height="250px" width="250px"></p> 
          </td>
        </tr>

        @foreach( $siswas as $siswa)
        @if($siswa->id == $id)
        <tr>
          <td colspan="4" style="font-size: 17px; text-align: justify;">
            <p>Status Mahasiswa : {{$siswa->status}} </p>
          </td>
        </tr>
        @endif
        @endforeach
        
      @endif
      @endforeach
    </table>

</main>
 
</body>
</html>