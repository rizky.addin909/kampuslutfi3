@extends('../layout/applog')

@section('title','Nama Website')


<!-- Page content -->
@section('content')
  <!-- Header -->
  <header class="w3-container" style="padding-top:22px">
    <h4><span>Data <strong>User</strong></span><br><h4>
  </header>

  <div class="w3-panel">
    <div class="w3-row-padding" style="margin:0 -16px">
      <div class="w3-twothird">
        <h5>Feeds</h5>
        <table class="w3-table w3-striped w3-white">
          @foreach( $users as $user)
          @if($user->id == $id)
              <form action="{{route('edituserupdate')}}" method="post">
                @csrf
                <input id="iduser" type="hidden"  name="iduser" value="{{$user->id}}">
                <tr>
                  <td>
                    <p>Nama</p>
                    <input type="text" name="username" id="username" value="{{$user->name}}">
                  </td>
                </tr>
                
                <tr>
                  <td>
                    <p>Email</p>
                    <input type="text" name="email" id="email" value="{{$user->email}}">
                  </td>
                </tr>

                <tr>
                  <td>
                    <p>Status Akun</p>
                    {{$user->status}}
                  </td>
                </tr>
                
                <tr>
                  <td>
                    @foreach( $siswas as $siswa)
                    @if($siswa->user_id == $id)
                    <p>Status Mahasiswa</p>
                    <input type="text" name="stat" id="stat" value="{{$siswa->status}}">
                    @endif
                    @endforeach
                  </td>
                </tr>

                <tr>
                  <td>
                    <input type="submit" value="ubah">  
                  </td>
                </tr>
                
              </form>
          @endif
          @endforeach
          
        </table>
      </div>
    </div>
  </div>
  @endsection

  @section('footer')
  <!-- Footer -->
  <footer class="w3-container w3-padding-16 w3-light-grey">
    <p>Created by </p>
  </footer>
@endsection
