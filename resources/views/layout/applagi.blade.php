<!DOCTYPE HTML>
<html>
<head>  
<title>Politeknik Perkebunan Yogyakarta</title>
<link rel="shortcut icon" href="image/background/poster-lpp.png" type="image/png">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Elite User Forms template Responsive, Login form web template,Flat Pricing tables,Flat Drop downs Sign up Web Templates, 
 Flat Web Templates, Login sign up Responsive web template, SmartPhone Compatible web template, free web designs for Nokia, Samsung, LG, SonyEricsson, Motorola web design">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Custom Theme files -->
<link href="../../css/font-awesome.css" rel="stylesheet"> 
<link href="../../css/popup-box.css" rel="stylesheet" type="text/css" media="all" />
<link href="../../css/style.css" rel='stylesheet' type='text/css' />
<!--fonts--> 
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Questrial" rel="stylesheet">
<!--//fonts--> 
</head>
<body>
    <div class="agileinfo-dot">
        <!--background-->
        <!-- login -->
        <h1><img src="/image/background/poster-lpp.png" height="60px" width="300px"></h1>

	<p style="color: white;">silahkan login atau register dengan email pribadi</p>
<br>
        @yield('content')
            
    </div>
    @yield('footer')
    <!--js-->
        <script type="text/javascript" src="../../js/jquery-2.1.4.min.js"></script>
    <!--//js-->
    <!--popup-js-->
    <script src="../../js/jquery.magnific-popup.js" type="text/javascript"></script>
    <script>
                        $(document).ready(function() {
                        $('.popup-with-zoom-anim').magnificPopup({
                            type: 'inline',
                            fixedContentPos: false,
                            fixedBgPos: true,
                            overflowY: 'auto',
                            closeBtnInside: true,
                            preloader: false,
                            midClick: true,
                            removalDelay: 300,
                            mainClass: 'my-mfp-zoom-in'
                        });
                                                                                        
                        });
    </script>
    <!--//popup-js-->
    </body>
</html>
